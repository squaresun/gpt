package main

import (
	"strings"

	"golang.org/x/tools/go/packages"
)

// List missing package dependencies of input repo
func list(repo string) ([]string, error) {
	// Use map, which keys are package fullname, to avoid duplicated packages
	domainPkgsMap := map[string]bool{}

	pkgs, err := packages.Load(&packages.Config{Mode: packages.NeedDeps | packages.NeedImports}, repo+"/...")
	if err != nil {
		return nil, err
	}

	packages.Visit(pkgs, nil, func(pkg *packages.Package) {
		pkgSep := strings.Split(pkg.String(), "/")
		if len(pkgSep) < 3 || pkgSep[0] != domain {
			// Our convention of repo should be: grandpeak.com/{ORGANIZATION}/{REPO}
			// Don't handle the repo which don't follow this convention
			return
		}
		domainPkgsMap[strings.Join(pkgSep[:3], "/")] = true
	})

	domainPkgs := []string{}
	for name := range domainPkgsMap {
		// Check if installed
		if _, ok := installed[name]; ok {
			continue
		}
		info("Depends on: %s", name)
		domainPkgs = append(domainPkgs, name)
	}

	return domainPkgs, nil
}
