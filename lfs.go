package main

import (
	"os/exec"
)

// TODO: support environment without git-lfs
func lfsPull(dir string) error {
	err := lfsCMD("fetch", dir)
	if err != nil {
		return err
	}
	return lfsCMD("checkout", dir)
}

func lfsCMD(arg, dir string) error {
	cmd := exec.Command("git", "lfs", arg)
	cmd.Dir = dir
	out, err := cmd.CombinedOutput()
	info(string(out))
	return err
}
