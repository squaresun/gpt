package main

import (
	"fmt"
	"io/ioutil"
)

var outWriter = ioutil.Discard

var (
	blue   = "34"
	yellow = "33"
	green  = "32"
	red    = "31"
)

func loggerGen(color string) func(format string, args ...interface{}) {
	return func(format string, args ...interface{}) {
		outWriter.Write([]byte(fmt.Sprintf("\x1b[%s;1m%s\x1b[0m\n", color, fmt.Sprintf(format, args...))))
	}
}

// info should be used to describe the example commands that are about to run.
var info = loggerGen(blue)

// warning message
var warn = loggerGen(yellow)

// success should be used to describe the commands are executed successfully.
var success = loggerGen(green)
