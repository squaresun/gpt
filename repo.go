package main

import (
	"fmt"
	"os"
	"path"
	"strings"

	git "gopkg.in/src-d/go-git.v4"
)

const domain = "grandpeak.com"

const url = "ssh://git@192.168.135.218:8822"

// Handles Both grandpeak.com and ssh://git@192.168.135.218:8822
var urlMap = map[string]string{
	domain: url,
	// Handling if you directly copied from gitea url
	url: url,
}

var domainMap = map[string]string{
	domain: domain,
	// Handling if you directly copied from gitea url
	url: domain,
}

// Update with remote
var remoteName = "origin"

// Transform {organization}/{repo} to {domain}/{organization}/{repo}
func toFullRepoPath(repos []string) error {
	for i, repo := range repos {
		vars := len(strings.Split(repo, "/"))
		if vars < 2 {
			return fmt.Errorf("Invalid repo: %s", repo)
		}
		if vars == 2 {
			repos[i] = fmt.Sprintf("%s/%s", domain, repo)
		}
	}

	return nil
}

func clone(localPath string) (*git.Repository, string, error) {
	domainName := strings.Split(localPath, "/")[0]
	// The absolute localPath
	goLocalPath := path.Join(os.Getenv("GOPATH"), "src", localPath)
	proj, err := git.PlainOpen(goLocalPath)
	if err == nil {
		warn("You've already cloned %s", localPath)
		return proj, goLocalPath, nil
	}
	if err == git.ErrRepositoryNotExists {
		urlPath := strings.Replace(localPath, domainName, urlMap[domainName], -1)
		info("git clone %s", localPath)
		_, err = git.PlainClone(goLocalPath, false, &git.CloneOptions{
			URL:               urlPath,
			Progress:          outWriter,
			RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
		})
		if err != nil {
			return nil, goLocalPath, err
		}

		// Git lfs pull
		return proj, goLocalPath, lfsPull(goLocalPath)
	}
	return proj, goLocalPath, err
}

func updateRepo(proj *git.Repository, path string) error {
	tree, err := proj.Worktree()
	if err != nil {
		return err
	}
	// Pull the latest changes from the origin remote and merge into the current branch
	remote, err := proj.Remote(remoteName)
	if err != nil {
		return err
	}

	info("git pull %s", remote.Config().URLs[0])
	err = tree.Pull(&git.PullOptions{
		RemoteName:        remoteName,
		Progress:          outWriter,
		RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
	})
	if err != nil {
		if err != git.NoErrAlreadyUpToDate {
			return err
		}
		// repo is already up to date
		// ignore this error and continue
		warn(err.Error())
		return nil
	}

	// Git lfs pull
	err = lfsPull(path)
	if err != nil {
		return err
	}

	// Print the latest commit that was just pulled
	ref, err := proj.Head()
	if err != nil {
		return err
	}

	commit, err := proj.CommitObject(ref.Hash())
	if err != nil {
		return err
	}

	info(commit.String())

	success("git pull %s success", remote.Config().URLs[0])

	return nil
}

func invalidDomain(d string) bool {
	return strings.Contains(d, "/")
}
