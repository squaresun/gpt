package main

var installed = map[string]bool{}

func install(repos []string, update bool) error {
	for _, repo := range repos {
		// Check if installed
		if _, ok := installed[repo]; ok {
			continue
		}

		installed[repo] = true
		proj, path, err := clone(repo)
		if err != nil {
			return err
		}

		if update {
			err = updateRepo(proj, path)
			if err != nil {
				return err
			}
		}

		// Recursively resolve dependencies
		// List and install missing dependencies under current repo
		l, err := list(repo)
		if err != nil {
			return err
		}

		err = install(l, update)
		if err != nil {
			return err
		}
	}

	return nil
}
