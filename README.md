# GPT

The package fetch tool under our [Git server](http://192.168.135.218:8800)

```shell
$ gpt -help
Usage of gpt:
  -p string
        Go files path, as the entry point of resolving dependencies (default "./...")
  -u    Update existing git repo
  -v    Verbose output, i.e. print out progress
```

## Installation

```shell
go get -u gitlab.com/squaresun/gpt
```

## Dependency

### git-lfs

This project depends on [git-lfs](https://git-lfs.github.com/). Please make sure you have it installed as `Git` cli extension.

### SSH agent

If you use ssh to fetch repo, please make sure you have `ssh-agent` process running. You may check with:

```shell
$ ps -p $SSH_AGENT_PID
  PID TTY          TIME CMD
 1370 ?        00:00:00 ssh-agent

# or if ps: unrecognized option: p
$ ps | grep ssh-agent
   10 root      0:00 ssh-agent  # ssh-agent process
   14 root      0:00 grep ssh-agent
```

If it isn't running, run:

```shell
eval "$(ssh-agent -s)"
```

And make sure you added your private key, e.g. your private located in `~/.ssh/id_rsa`:

```shell
ssh-add ~/.ssh/id_rsa
```

## Example

### Fetch all missing dependency

```shell
gpt
```

For example, you have a folder `hello-world` contains a go program:

```yaml
hello-world
- main.go
- app.go
- hello.go
- ...
```

and you want to fetch all missing `grandpeak.com` packages, `cd` into folder `hello-world` and runs `gpt`

### Fetch 1 or more package(s)

```shell
gpt grandpeak.com/pivsti/core grandpeak.com/jyd/math
# or just
gpt pivsti/core jyd/math
```

### Update 1 or more package(s)

```shell
gpt -u grandpeak.com/pivsti/core grandpeak.com/jyd/math
```

### Update 1 or more package(s), verbose output

```shell
gpt -u -v grandpeak.com/pivsti/core grandpeak.com/jyd/math
```

### Explicitly define directory to resolve dependencies

```shell
# Resolving dependencies on ./cmd/server
gpt -p ./cmd/server
```
