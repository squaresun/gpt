package main

import (
	"flag"
	"log"
	"os"
)

func main() {
	if err := gpt(); err != nil {
		log.Fatal(err)
	}
}

func gpt() error {
	var repos []string
	execPath := flag.String("p", "./...", "Executable package path, use this if no repo specified")
	verbose := flag.Bool("v", false, "Verbose output, i.e. print out progress")
	update := flag.Bool("u", false, "Update existing git repo")
	flag.Parse()

	repos = flag.Args()

	if *verbose {
		outWriter = os.Stdout
	}

	var err error
	if len(repos) <= 0 {
		// Get all missing packages
		repos, err = list(*execPath)
	} else {
		err = toFullRepoPath(repos)
	}
	if err != nil {
		return err
	}

	err = install(repos, *update)
	if err != nil {
		return err
	}

	return nil
}
